from django.contrib import admin
from django.urls import path, include
from receipts.views import all_receipts
from accounts.views import user_login, user_logout, signup
from django.views.generic.base import RedirectView

urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signup, name="signup"),
]
